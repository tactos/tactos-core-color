# Tactos-core-color

Tactos-core is a linux application that explore the screen close to the mouse pointer in order to give feedback according to the color detected. 

![Tactos-core usage example](doc/imgs/demo.gif)

## How does it work ?

Tactos-core has two ways of feedback. 

- **speech-synthesis** : when a color is detected, the application can send a speech to a speech synthesis server. Speech can be triggered immediatly or by specific event like mouse click.
- **processed matrix** : the part of the screen explored is split into cells. Each cell is evaluate to find colors. Then a boolean matrix is created with the result. This matrix aims to be transfered to a braille tool.

![Tactos-core processing example](doc/imgs/core_ex.png)

## Building

### Dependencies


 - g++
 - meson 
- libx11-dev
- libxtst-dev
- pkg-config
- libglibmm-2.4-dev
- libyaml-cpp-dev 
- libopencv-dev
- libspeechd-dev (optional for audio synthesis support)

### Compilation

 Tactos-MIT4 uses [meson](https://mesonbuild.com/) as build system.

```bash
meson --buildtype=release builddir 
cd builddir
ninja
```

### Installation

```bash
cd buildir
ninja Install
```

### Regenerating Dbus interface

Dbus interface is generated through [gdbus-codegen-glibmm](https://github.com/Pelagicore/gdbus-codegen-glibmm)
To regenerate the interface run the followin commands : 

```
    gdbus-codegen-glibmm2 --generate-cpp=tactosColor DBUS_interface.xml
    mv tactosColor*.h include/
    mv tactosColor*.cpp src/
```

### Documentation

A doxygen documentation can be found [here](https://tactos.gitlab.io/tactos-core-color/)

## Contact

This project is developed by the the laboratory of [Costech](http://www.costech.utc.fr/) in the [University of Technology of Compiegne (UTC)](https://www.utc.fr/) in partnership with [hypra](http://hypra.fr/-Accueil-1-.html) and [natbraille](http://natbraille.free.fr/).




