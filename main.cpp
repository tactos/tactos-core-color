#include "tactosIPC_proxy.h"
#include "config.h"
#include "xeventrecorder.h"
#include "screenrecorder.h"
#include <fstream>
#include <glibmm/miscutils.h>
#include <sys/stat.h>
#include <functional>
#include <array>
#include <filesystem>
#include <tactosColor_impl.h>
#include <sys/types.h>
#include<X11/XKBlib.h>



#include<thread>

#define IPC_WELL_KNOWN_NAME "org.tactos.ipc"
#define IPC_WELL_KNOWN_OBJECT "/org/tactos/ipc"
#define CONFIG_WELL_KNOWN_NAME "org.tactos.color"
#define CONFIG_WELL_KNOWN_OBJECT "/org/tactos/color"
#define DEFAULT_CONF_FILE "NB.yml"

using namespace Tactos;
using namespace std::placeholders;
typedef std::filesystem::path path;

/**
 * @brief Separate thread to initate DBus mainloop to expose the configuration interface
 * 
 * @param record ScreenRec object to update.
 */
void dbusInit (std::shared_ptr<ScreenRec> record)
{
    // Instantiate and run the main loop
    Glib::RefPtr<Glib::MainLoop> ml = Glib::MainLoop::create();
    
    // Connect to the system bus and acquire our name
    TactosColorImpl config_interface(record);
    guint connection_id = Gio::DBus::own_name(
        Gio::DBus::BUS_TYPE_SESSION,
        CONFIG_WELL_KNOWN_NAME,
        [&](const Glib::RefPtr<Gio::DBus::Connection> &connection,
            const Glib::ustring & name ) {
            std::cout << "Connected to bus : " << name << std::endl;
            if (config_interface.register_object(connection, CONFIG_WELL_KNOWN_OBJECT) == 0)
                ml->quit();
            },
        [&](const Glib::RefPtr<Gio::DBus::Connection> & /* connection */,
            const Glib::ustring & name) {
            std::cout << "Name " << name << " acquired.\n";
            },
        [&](const Glib::RefPtr<Gio::DBus::Connection> & /* connection */,
            const Glib::ustring & name) {
            std::cout << "Name " << name << " lost.\n";
            ml->quit();
            });
    
    ml->run();
    Gio::DBus::unown_name(connection_id);
}

/**
 * @brief Callback function to display on terminal when a reader join or quit
 * 
 * @param proxy DBus proxy
 * @param reader_nbr Number of readers.
 */
void UpdateReader(const  Glib::RefPtr<org::tactos::ipcProxy> proxy, std::shared_ptr<uint> reader_nbr)
{
    uint rdr = proxy->Reader_get();
    if (rdr > *reader_nbr)
    {
        std::cout << "New reader joined now " << rdr << " readers" << std::endl;
    } else
    {
        std::cout << "reader left still " << rdr << " readers" << std::endl;
    }
    *reader_nbr = rdr;
}

/**
 * @brief callback function to call ScreenRec object to update the size of the screenshot matix.
 * 
 * @param recorder Screen rec object
 * @param proxy DBus proxy
 */
void updateSize(std::shared_ptr<ScreenRec> recorder, const  Glib::RefPtr<org::tactos::ipcProxy> proxy)
{
    auto size = proxy->Size_get();
    recorder->setMatrixSize(std::get<0>(size), std::get<1>(size));
}

/**
 * @brief look for file in default locations.
 * 
 * @param filename filename to find [default:NB.yml]
 * @return path found
 * 
 */
std::optional<path> getFilePath(std::string filename = DEFAULT_CONF_FILE)
{
    
    path file;
    std::string dataDirs = std::getenv("XDG_DATA_DIRS");
    if (dataDirs.empty())
    {
    dataDirs = "/usr/local/share/:/usr/share/";
    }

    std::vector<path> default_paths;
    default_paths.push_back(path());
    size_t pos = 0;
    while ((pos = dataDirs.find(':')) != std::string::npos) {
        default_paths.push_back(path(dataDirs.substr(0, pos)) /= "tactos");
        dataDirs.erase(0, pos + 1);
    }

    for (auto path : default_paths)
    {
        file = path /= filename;
        if (std::filesystem::is_regular_file(file))
        {
            return file;
        }
    }
    return {};
}

void cancelSpeechClbk (std::shared_ptr<Tactos::ScreenRec> rec, int, int)
{
        rec->cancel_speech();
}

void mit_button(std::shared_ptr<Tactos::ScreenRec> rec, Tactos::SPEECH_TRIGGER trigger, int, int)
{    
    std::cout << "mit button pressed" << std::endl;
    rec->sayCurrentSentence(trigger);
}

/**
 * @brief look for 2 unused keycodes tu use it for trigger mit5 buttons.
 * 
 * To synchronize  keycodes used between mit5 and tactos-core-color, the same function is run 
 * on both side.
 * This funtion choose the first and the last free keycode given by the command "xmodmap -pke"
 * 
 */
std::pair<uint8_t, uint8_t> get_unused_keycode() {
    std::array<char, 128> buffer;
    std::string result;
    std::unique_ptr<FILE, decltype(&pclose)> first_pipe(popen("xmodmap -pke | grep -E \"[0-9]{3} =$\" | cut -d \" \" -f 2 | tail -n 1", "r"), pclose);
    if (!first_pipe) {
        throw std::runtime_error("popen() failed!");
    }
    while (fgets(buffer.data(), buffer.size(), first_pipe.get()) != nullptr) {
        result += buffer.data();
    }
    uint8_t first_keycode = std::stoul(result);
    result.clear();
    
    std::unique_ptr<FILE, decltype(&pclose)> last_pipe(popen("xmodmap -pke | grep -E \"[0-9]{3} =$\" | cut -d \" \" -f 2 | head -n 1", "r"), pclose);
    if (!last_pipe) {
        throw std::runtime_error("popen() failed!");
    }
    while (fgets(buffer.data(), buffer.size(), last_pipe.get()) != nullptr) {
        result += buffer.data();
    }
    uint8_t last_keycode = std::stoul(result);
    std::cout << "found key codes " << std::to_string(first_keycode) << " and " << std::to_string(last_keycode) << std::endl;
    return {first_keycode, last_keycode};
}


int main(int argc, char * argv[])
{   
    std::optional<path> file;
    std::string fileName;
    if(argc >= 2)
    {
        file = getFilePath(argv[1]);
    } else
    {
        file = getFilePath();
    }
    
    Configuration conf = file.has_value( ) ? Configuration(file.value()) :Configuration();
    
    Glib::init();
    Gio::init();
    std::cout << "launching dbus service" << std::endl;
    Glib::RefPtr<org::tactos::ipcProxy> proxy;
    proxy = org::tactos::ipcProxy::createForBus_sync(
        Gio::DBus::BUS_TYPE_SESSION,
        Gio::DBus::PROXY_FLAGS_NONE,
        IPC_WELL_KNOWN_NAME,
        IPC_WELL_KNOWN_OBJECT,
        Gio::Cancellable::create()
    );
    
    std::pair<int, int> winSize = conf.getWinSize();
    std::tuple<guchar,guchar> mat = proxy->Size_get();
    std::shared_ptr<ScreenRec> recorder = std::make_shared<ScreenRec>(
        mat_size(winSize.first ,winSize.second),
        mat_size(std::get<0>(mat), std::get<1>(mat)));
    
    recorder->setRangeList(conf.getColors()) ;
    recorder->ipc_signal.connect(sigc::mem_fun(proxy.get(), &org::tactos::ipcProxy::Matrix_set_sync));
    
    pid_t pid = getpid();
    if (not proxy->publisherJoin_sync(pid))
    {
        throw std::runtime_error("A publisher already exist to tactos");
    }
    proxy->Size_changed().connect(std::bind(updateSize, recorder, proxy));
    // take care of the number of reader
    std::shared_ptr<uint> reader_nbr = std::make_shared<uint>(proxy->Reader_get());
    std::cout << "there is currently " << *reader_nbr << " reader" << std::endl;
    proxy->Reader_changed().connect(std::bind(UpdateReader, proxy, reader_nbr));
    
    XEventRecorder record = XEventRecorder(*recorder.get(), {MotionNotify, KeyPress, ButtonPress});
    auto keycodes = get_unused_keycode();
    
    record.addKeyEvent(keycodes.first, std::bind(mit_button, recorder, Tactos::SPEECH_TRIGGER::MIT_BUTTON_1, _1, _2));
    record.addKeyEvent(keycodes.second, std::bind(mit_button, recorder, Tactos::SPEECH_TRIGGER::MIT_BUTTON_2, _1, _2));
    record.addKeyEvent(XStringToKeysym("Control_L"), std::bind(cancelSpeechClbk, recorder, _1, _2));
    
    auto creation = std::make_pair(Mod5Mask  ,XStringToKeysym("space"));
    auto next = std::make_pair(Mod5Mask,XStringToKeysym("j"));
    auto prev = std::make_pair(Mod5Mask,XStringToKeysym("f"));
    record.addCheckpointSupport(creation, next, prev);
    
    std::thread dbus(dbusInit, recorder);
    std::cout << "Start recording, press 'Escape' to stop" << std::endl;
    record.startRecord();
  
    proxy->publisherLeave_sync();
      
    return 0;
  }
