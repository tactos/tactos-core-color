

# Tactos-core-color

This document aims to explain `Tactos-core-color` architecture. If you only want to use it, please refer to [README.md](../README.md).

## Functionnement principle

`Tactos-core-color` is directly interfaced with linux X server. This behavior allows to be application agnostic. The interface with X server is done through  [Xlib](https://tronche.com/gui/x/xlib/) and use the extension [xrecord](https://www.x.org/releases/X11R7.6/doc/libXtst/recordlib.html) to capture X event without interfering with applications.

## Event processing

The C++ class in charge of listening event is `XEventRecorder`. This class listen 3 distincts events :

- `MotionNotify` : mouse movement event. This event is send to `Screenrec` class that capture sreenshot around mouse pointer, split it into 16 cellsthen look for registered color in each cell in order to compute the resulting matrix representing braille pins.

![schéma de traitement d'un événement MotionNotify](imgs/tactos_core_color_mouse.svg)

- `ButtonPress` : mouse button event. This event is send to `Screenrec` class and look for a sentence buffered to say with TTS engine.

![schéma de traitement d'un événement ButtonPress](imgs/tactos_core_color_button.svg)

- `KeyPress` : Look for a callback registered with this keypress combinaison

![schéma de traitement d'un événement KeyPress](imgs/tactos_core_color_key.svg)

