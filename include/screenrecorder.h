#pragma once


#include <memory>
#include <functional>
#include <unordered_set>

#include <opencv2/opencv.hpp>
#include <X11/Xlib.h>

#include <X11/extensions/XShm.h>
#include <sys/shm.h>
#include <sigc++/sigc++.h>
#include <filesystem> 

#ifdef SPEECH_LIB // compile with speechd support only if present
#include<libspeechd.h>
#endif // SPEECH_LIB

#include "struct.h"

namespace Tactos {

    typedef std::pair<bool, std::unordered_set<rangeColor>> rangeResult;
    typedef std::pair<bool, std::string> speech_t;
    typedef std::unordered_map< uint, std::unordered_map< Tactos::rangeColor, uint8_t >> speechs_weight_t ; ///< store speech weight for each range for each trigger type
  /**
  * @brief screenshot informations
  * 
  * @details 
  *  Structure to defined the captured area of the screen.
  *  the global area is defined by the up left point (\ref orig_x; \ref orig_y)
  *  and \ref width and \ref height variables.
  * 
  *  Nevertheless, part of the area can be out of the screen. 
  *  In order to avoid trying capture out of the screen pixel, 
  *  borders variables, define optional part that don't have to
  *  be captured but fill with a default value defined in \ref ScreenRec class
  * 
  */
  struct scrshot_info {
    int orig_x; ///< x left high corner coordinate
    int orig_y; ///< y left high corner coordinate
    int width;  ///< width of the captured screenshot
    int height; ///< height of the captured screenshot
    int border_t = 0; ///< top border to not capture
    int border_b = 0; ///< bottom border to not capture
    int border_l = 0; ///< left border to not capture 
    int border_r = 0; ///< right border to not capture
  };
  
  /**
  * @brief size of a rectangle element defined by width and height.
  * 
  */
  struct mat_size {
    uint width;///< width of the rectangle
    uint height;///< height of the rectangle
        
    /**
    * @brief explicit constructor
    * 
    * @param w width
    * @param h height..
    */
    mat_size(uint w, uint h):width(w), height(h){};
    /**
    * @brief default constructor. Initialize width and height with 0
    * 
    */
    mat_size():width(0), height(0){};
  };

  /**
  * @brief class in charge of taking screenshot and analyzing it 
  * to get the boolean matrix and text to say.
  */
  class ScreenRec{
  public:
    ScreenRec(mat_size screenshot, mat_size matrix = mat_size(4,4), cv::Scalar t_defaultValue = cv::Scalar(255,255,255,0));
    ~ScreenRec();
    bool updateConf(std::filesystem::path file);
    void capture(int x, int y);
    void scrSplit(uint n);
    void scrSplit(uint lines, uint cols);
    std::vector<bool> applyToGrid(std::function<rangeResult (cv::Mat, bool)> func = nullptr);
    rangeResult isInRange(cv::Mat mat, bool any = true) const;
    
    bool removeRange(const rangeColor oldColor);
    bool updateRange(const rangeColor updateColor);
    bool addRange(rangeColor newScalar);
    void setRangeList(std::set< Tactos::rangeColor > colorList);
    std::set<Tactos::rangeColor> getRangeList();
    inline void setDefaultValue(cv::Scalar value);
    void setMatrixSize(uint cols, uint lines = 4);
    void setScreenshotSize(uint w, uint h);
    void setScreenshotSize(uint n);
    std::pair<uint, uint> getScreenshotSize();
    sigc::signal<void, std::vector<bool>> ipc_signal;
    void processScreenshot(int x, int y);
    void sayCurrentSentence(SPEECH_TRIGGER type = MOUSE_CLICK);
    void saySentencesList(std::vector<speech_t> sentences);
    void saySentence(std::string sentence, bool sound = false);
    std::array<speech_t, TRIGGER_SIZE> getCurrentSentences();
    void cancel_speech();
  private:
    bool partOutOfScreen(std::shared_ptr<scrshot_info> captInfo);
    void sayImmediateSentence();
    void updateSentences(speechs_weight_t& speechs_weight);
    
    mat_size scrshot_size; ///< size of the screenshot
    mat_size matrix_size; ///< size of the the result matrix
    mat_size screen_size; ///< size of the screen

    // Xlib screen informations
    Display* m_disp;
    Window m_root;
    Screen m_screen;

    // shared memory screenshot informations
    XImage* m_Ximg;
    XShmSegmentInfo m_XshmInfo;

    
    cv::Mat m_screenshot; ///< screenshot with size of #scrshot_size
    cv::Scalar m_defaultValue;
    
    
    Bool m_any; ///< detection valid if any pixel inside a range (True) or all of them (false)
    std::vector<cv::Mat> m_subScreenShot; ///< array with  16 part of the screeshot
    

    
    // list of range to detect
    std::set<rangeColor> m_rangeList;
    
    // last speech said to avoid speech repetition
    std::string lastSpeech;
    
    // sentence to say if requested
    std::array<speech_t, TRIGGER_SIZE> speech_sentence;

#ifdef SPEECH_LIB // compile with speechd support only if present    
    // connection to speech syntesis
    SPDConnection* speechCon_current;
    SPDConnection* speechCon_delayed;
#endif // SPEECH_LIB
  };
}
