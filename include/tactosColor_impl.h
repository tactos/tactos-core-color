#pragma once
#include "tactosColor_stub.h"
#include "screenrecorder.h"

/**
 * @brief DBUS representation of rangeColor : (y(uu)byss)
 * 
 */
typedef   std::tuple<uint16_t, std::tuple<guint32, guint32>, bool, uint8_t, Glib::ustring, Glib::ustring, bool> colorTuple_t;

/**
 * @brief Implementation of tactos-core-color DBus interface to be able to configure it through DBus. It 
 * implemente colorStub generated interface from glibmm-codegen.
 * 
 */
class TactosColorImpl : public org::tactos::colorStub {
public:
    TactosColorImpl(std::shared_ptr<Tactos::ScreenRec> recorder);
    
    void loadConfigFile( const Glib::ustring & file, org::tactos::colorStub::MethodInvocation & invocation ) override;
    
    void removeColor ( const std::tuple<guint32,guint32> & color, org::tactos::colorStub::MethodInvocation & invocation ) override;
    void addColor ( uint16_t id, const std::tuple<guint32,guint32> & color, bool pins, uint8_t speech_trigger, const Glib::ustring & speech, const Glib::ustring & name, bool sound_icon, org::tactos::colorStub::MethodInvocation & invocation ) override;
    void updateColor(uint16_t id, const std::tuple<guint32, guint32> & color, bool pins, uint8_t speech_trigger,  const Glib::ustring & speech, const Glib::ustring & name, bool sound_icon, org::tactos::colorStub::MethodInvocation & invocation) override;
    
    void loadConfigList(const std::vector<colorTuple_t > & list, org::tactos::colorStub::MethodInvocation & invocation) override;
    
    void getColors ( org::tactos::colorStub::MethodInvocation & invocation ) override;
    std::tuple<guint8, guint8> detectionWin_get() override;
    bool detectionWin_setHandler ( const std::tuple<guint8, guint8> & value ) override;
    
private:
    std::shared_ptr<Tactos::ScreenRec> m_recorder;
};
