#pragma once

#include <vector>
#include <memory>
#include<map>
#include"struct.h"

#include <X11/Xlib.h>
#include <X11/extensions/record.h>
#include <X11/Xlibint.h>
#undef max // Hack to be able to use stdmin and max 
#undef min // instead of Xlibint ones

#include"screenrecorder.h"
#include"shortcutbuffer.h"

namespace Tactos {

    typedef std::function<void(int, int)> event_clbk;
    typedef std::pair<uint16_t, KeyCode> shortcut_t;
    typedef unsigned int record_event_t ;
  /**
  * @brief connecte to X server and record events. 
  * 
  * @details Record by default MotionEvent and KeyPressEvent.
  * 
  */
  class XEventRecorder {
  public:
    XEventRecorder(ScreenRec& t_screenrecorder, std::vector<unsigned int> t_recEv = {MotionNotify, KeyPress}, int t_datumFlags = XRecordFromClientTime | XRecordFromClientSequence, XRecordClientSpec t_clts = XRecordAllClients, std::string t_exitKey = "Escape");
      ~XEventRecorder();
      void startRecord();
      void startRecord(void* callbackData);
      void stopRecord();
      void addKeyEvent(KeyCode key, event_clbk clbk, uint8_t key_mod =0 );
      void addKeyEvent(KeySym key, event_clbk clbk, uint8_t key_mod =0 );
      void addCheckpointSupport(shortcut_t creation, shortcut_t next, shortcut_t prev);
      XRecordContext getContext();
      void addRecEvent(record_event_t ev);
      Display* getDisplay();
      
  private:
       enum BUTTON_CODE {
          LEFT = 260,
          CENTER = 516,
          RIGHT = 772
      };
      static void eventCallback(XPointer data, XRecordInterceptData* ev);
      void moveMouse(int new_x, int new_y, int x, int y);
      void moveMouseTocheckpoint(int x, int y, bool prev = true);
      void addCheckpoint(int x, int y);
      void stopRecord_clbk(int , int);
      bool isDeviceEvent(record_event_t ev);
      bool isInRangeOrAdjacent(record_event_t ev, const XRecordRange8& range);
      void callKeyEvent(int x, int y, shortcut_t key);
      Display* XControlConnexion; // control connection with X server
      Display* XRecordConnection; // record connection with X server
      XRecordContext controlContext;
      XRecordContext recordContext;
      ScreenRec& screenrecorder;
      std::map<shortcut_t, event_clbk> keyEvents;
      
      CheckpointBuffer<10> checkpoints;
      
      XRecordClientSpec clients;
      
  };
}
