#pragma once
#include <opencv2/opencv.hpp>
#define TRIGGER_SIZE 6
namespace Tactos {
    
      
    /**
     * @brief Type of speech triggering
     * 
     */
    enum SPEECH_TRIGGER {
   IMMEDIATE, ///< trigger immediatly without waiting for event
   MOUSE_CLICK, ///< trigger on any mouse click
   LEFT_CLICK, ///< trigger on left click
   MIDDLE_CLICK, ///< trigger on middle click
   MIT_BUTTON_1, ///< trigger on first MIT5 button
   MIT_BUTTON_2 ///< trigger on second MIT5 button
};
  /**
   * @brief struct to store range of colors (stored as cv::scalars).
   * 
   */
    /**
    * @brief range color constructor
    * 
    * @param t_start Starting values of the range. a value of -1 for a field is equivalent
    * to 0 for start and 255 for end (don't care).
    * @param t_end Ending values of the range.
    * @param t_name name associated to the range that will be said by speech synthesis
    * @param t_cells True if detecting this range raise pin
    * @param t_immediate True if speech should be triggered immediatly
    * @param t_index unique id of the color
    */
    struct rangeColor {
    rangeColor(cv::Scalar t_start, cv::Scalar t_end, std::string t_speech = "", bool t_pins = true, SPEECH_TRIGGER t_speech_trigger = IMMEDIATE, std::string t_name = "",  uint16_t t_index = 0, bool t_sound_icon = false) :start(t_start), end(t_end), speech(t_speech), pins(t_pins), speech_trigger(t_speech_trigger), name(t_name), index(t_index), sound_icon(t_sound_icon){};
    cv::Scalar start; ///< Starting values of the range. a value of -1 for a field is equivalent to 0 for start and 255 for end (don't care).
    cv::Scalar end;   ///< Ending values of the range.
    std::string speech; ///< name associated to the range that will be said by speech synthesis
    bool pins; ///< true if cellls must be triggered when the color is detected
    SPEECH_TRIGGER speech_trigger; ///< type of speech triggering
    std::string name; ///< name used by configuration software
    uint16_t index; ///< index to sort values
    bool sound_icon; ///< whether speech is  sound icon or not
    /**
    * @brief overloaded operator. check only if \ref start and \ref end are equal
    * 
    * @param second rangeColor to compare
    * @return bool
    */
    bool operator==(const rangeColor& second) const
    {
      return start == second.start && end == second.end;
    }
    
    bool operator<(const rangeColor& second) const
    {
        return index < second.index;
    }
    
    bool hasSpeech()
    {
        return !speech.empty();
    };
  };
}

// definition of rangeScalar hash function to use it in unordered_set
namespace std {
  template<>
  /**
   * @brief add hash specialisation for Tactos::rangeColor. Calcul hash of start and end values.
   * 
   */
  struct hash<Tactos::rangeColor> {
    inline size_t operator()(const Tactos::rangeColor& el) const {
      std::size_t hash = 0;
      for (int i = 0; i < 4 ; ++i)
      {
        hash ^= std::hash<double>{}(el.start[i]) + 0x9e3779b9 + (hash<<6) + (hash>>2);
        hash ^= std::hash<double>{}(el.end[i]) + 0x9e3779b9 + (hash<<6) + (hash>>2);
      }
        hash ^= std::hash<double>{}(el.index) + 0x9e3779b9 + (hash<<6) + (hash>>2);
      return hash;
    }
  };
}
