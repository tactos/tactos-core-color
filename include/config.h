#ifndef TACTOS_CONFIG_H
#define TACTOS_CONFIG_H

#pragma once

#include<tuple>
#include <unordered_set>
#include <opencv2/opencv.hpp>
#include<yaml-cpp/yaml.h>
#include "struct.h"


namespace Tactos {
  
  /**
  * @brief load and save configuration from/to file
  * 
  * @details if nothing is set, default values are : 
  * ~~~~~~~~~~~~~~~~{.yml}
  * window : 
  *   - width  : 32
  *   - height : 32
  * ~~~~~~~~~~~~~~~~
  */
  class Configuration {
  public:  
    Configuration(std::string t_fileName = "");
    ~Configuration();
    void saveConfiguration(std::string t_name);
    std::set<rangeColor> getColors();
    void setColors(std::set<rangeColor> colors);
    std::pair<uint, uint>  getWinSize();
    void  setWinSize(std::pair<uint, uint> t_winSize);
    void loadConfiguration();
    void saveConfiguration();
    
    
  private:
    

    std::string yamlName; 
    std::set<rangeColor> detectRanges;
    std::pair<uint, uint>  winSize;
    
  };
}


#endif // TACTOS_CONFIG_H
