#pragma once
#include <string>
#include <vector>
#include <glibmm.h>
#include <giomm.h>
#include "tactosColor_common.h"

namespace org {
namespace tactos {

class colorStub : public sigc::trackable {
public:
    colorStub();
    virtual ~colorStub();

    colorStub(const colorStub &other) = delete;
    colorStub(colorStub &&other) = delete;
    colorStub &operator=(const colorStub &other) = delete;
    colorStub &operator=(colorStub &&other) = delete;

    guint register_object(const Glib::RefPtr<Gio::DBus::Connection> &connection,
                          const Glib::ustring &object_path);
    void unregister_object();

    unsigned int usage_count() const {
        return static_cast<unsigned int>(m_registered_objects.size());
    }

    class MethodInvocation;

    bool detectionWin_set(const std::tuple<guchar,guchar> & value);
protected:
    virtual void addColor(
        guint16 id,
        const std::tuple<guint32,guint32> & color,
        bool pins,
        guchar speech_trigger,
        const Glib::ustring & speech,
        const Glib::ustring & name,
        bool sound_icon,
        MethodInvocation &invocation) = 0;
    virtual void updateColor(
        guint16 id,
        const std::tuple<guint32,guint32> & color,
        bool pins,
        guchar speech_trigger,
        const Glib::ustring & name,
        const Glib::ustring & speech,
        bool sound_icon,
        MethodInvocation &invocation) = 0;
    virtual void removeColor(
        const std::tuple<guint32,guint32> & color,
        MethodInvocation &invocation) = 0;
    virtual void loadConfigFile(
        const Glib::ustring & file,
        MethodInvocation &invocation) = 0;
    virtual void loadConfigList(
        const std::vector<std::tuple<guint16,std::tuple<guint32,guint32>,bool,guchar,Glib::ustring,Glib::ustring,bool>> & list,
        MethodInvocation &invocation) = 0;
    virtual void getColors(
        MethodInvocation &invocation) = 0;

    /* Handle the setting of a property
     * This method will be called as a result of a call to <PropName>_set
     * and should implement the actual setting of the property value.
     * Should return true on success and false otherwise.
     */
    virtual bool detectionWin_setHandler(const std::tuple<guchar,guchar> & value) = 0;
    virtual std::tuple<guchar,guchar> detectionWin_get() = 0;

    void on_method_call(const Glib::RefPtr<Gio::DBus::Connection> &connection,
                        const Glib::ustring &sender,
                        const Glib::ustring &object_path,
                        const Glib::ustring &interface_name,
                        const Glib::ustring &method_name,
                        const Glib::VariantContainerBase &parameters,
                        const Glib::RefPtr<Gio::DBus::MethodInvocation> &invocation);

    void on_interface_get_property(Glib::VariantBase& property,
                                   const Glib::RefPtr<Gio::DBus::Connection> &connection,
                                   const Glib::ustring &sender,
                                   const Glib::ustring &object_path,
                                   const Glib::ustring &interface_name,
                                   const Glib::ustring &property_name);

    bool on_interface_set_property(
        const Glib::RefPtr<Gio::DBus::Connection> &connection,
        const Glib::ustring &sender,
        const Glib::ustring &object_path,
        const Glib::ustring &interface_name,
        const Glib::ustring &property_name,
        const Glib::VariantBase &value);

private:
    bool emitSignal(const std::string &propName, Glib::VariantBase &value);

    struct RegisteredObject {
        guint id;
        Glib::RefPtr<Gio::DBus::Connection> connection;
        std::string object_path;
    };

    Glib::RefPtr<Gio::DBus::NodeInfo> introspection_data;
    std::vector<RegisteredObject> m_registered_objects;
    std::string m_interfaceName;
};

class colorStub::MethodInvocation {
public:
    MethodInvocation(const Glib::RefPtr<Gio::DBus::MethodInvocation> &msg):
        m_message(msg) {}

    const Glib::RefPtr<Gio::DBus::MethodInvocation> getMessage() {
        return m_message;
    }

    void ret(Glib::Error error) {
        m_message->return_error(error);
    }

    void returnError(const Glib::ustring &domain, int code, const Glib::ustring &message) {
        m_message->return_error(domain, code, message);
    }

    void ret(bool p0) {
        std::vector<Glib::VariantBase> vlist;
        Glib::Variant<bool> var0 =
            Glib::Variant<bool>::create(p0);
        vlist.push_back(var0);

        m_message->return_value(Glib::Variant<Glib::VariantBase>::create_tuple(vlist));
    }

    void ret() {
        std::vector<Glib::VariantBase> vlist;

        m_message->return_value(Glib::Variant<Glib::VariantBase>::create_tuple(vlist));
    }

    void ret(const std::vector<std::tuple<guint16,std::tuple<guint32,guint32>,bool,guchar,Glib::ustring,Glib::ustring,bool>> & p0) {
        std::vector<Glib::VariantBase> vlist;
        Glib::Variant<std::vector<std::tuple<guint16,std::tuple<guint32,guint32>,bool,guchar,Glib::ustring,Glib::ustring,bool>>> var0 =
            Glib::Variant<std::vector<std::tuple<guint16,std::tuple<guint32,guint32>,bool,guchar,Glib::ustring,Glib::ustring,bool>>>::create(p0);
        vlist.push_back(var0);

        m_message->return_value(Glib::Variant<Glib::VariantBase>::create_tuple(vlist));
    }

private:
    Glib::RefPtr<Gio::DBus::MethodInvocation> m_message;
};

} // tactos
} // org

