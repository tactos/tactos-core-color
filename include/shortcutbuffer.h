#pragma once

#include<string>
#include<array>
#include<optional>

namespace Tactos {
    /**
     * @brief checkpoint point saved structure 
     * 
     */
    struct checkpoint_t {
        
    checkpoint_t(){};
    checkpoint_t(int x, int y, std::pair<bool, std::string> speech):x(x), y(y), speech(speech){};
    int x;
    int y;
    std::pair<bool, std::string> speech;
    };

    /**
     * @brief store checkpoint in a circular buffer of fixed size.
     *  If the buffer is full, the new value overwrite the oldest one.
     * A navigation index allow to retrieve stored values.
     * 
     *  @param N size of the buffer
     */
    template<size_t N> class CheckpointBuffer {
    public:
        CheckpointBuffer():
        beginIndex(0), endIndex(0), navigationIndex(0){};
        void add(checkpoint_t checkpoint);
        const checkpoint_t& operator[](int index);
        const checkpoint_t get();
        const std::optional<checkpoint_t> next();
        const std::optional<checkpoint_t> prev();
        
    private:
        bool isEmpty();
        int nextIndex(uint8_t index);
        int prevIndex(uint8_t index);
        std::array< checkpoint_t, N> data;
        int beginIndex;
        int endIndex;
        int navigationIndex;
    };

    template<size_t N> bool CheckpointBuffer<N>::isEmpty()
    {
        return beginIndex == endIndex;
    }


    template<size_t N> int CheckpointBuffer<N>::nextIndex(uint8_t index)
    {
        return (index + 1) % N;
    }

    template<size_t N> int CheckpointBuffer<N>::prevIndex(uint8_t index)
    {
        return (index + N - 1) % N;
    }


    /**
     * @brief create new checkpoint. remove last one if full. Set navigation to this point.
     * 
     * @param N p_N:...
     * @param checkpoint p_checkpoint:...
     */
    template<size_t N> void CheckpointBuffer<N>::add(checkpoint_t checkpoint)
    {
    data[endIndex] = checkpoint;
    endIndex = nextIndex(endIndex); 
    navigationIndex  = endIndex;
    if (endIndex == beginIndex)
    {
        beginIndex = nextIndex(beginIndex);
    }
    }

    /**
     * @brief get current navigation point.
     * 
     * @param N p_N:...
     * @return const Tactos::checkpoint_t
     */
    template<size_t N> const checkpoint_t CheckpointBuffer<N>::get()
    {
        return data[navigationIndex];
    }

    template<size_t N> const checkpoint_t & CheckpointBuffer<N>::operator[](int index)
    {
        return data[index];
    }

    /**
     * @brief get next navigation point 
     * 
     * @param N p_N:...
     * @return const std::optional< Tactos::checkpoint_t >
     */
    template<size_t N> const std::optional<checkpoint_t> CheckpointBuffer<N>::next()
    {
        if(!isEmpty() and navigationIndex != endIndex and navigationIndex != prevIndex(endIndex))
        {
            navigationIndex = nextIndex(navigationIndex);
            return data[navigationIndex];
        }
        return {};
        
    }

    /**
     * @brief get previous navigation point.
     * 
     * @param N p_N:...
     * @return const std::optional< Tactos::checkpoint_t >
     */
    template<size_t N> const std::optional<checkpoint_t> CheckpointBuffer<N>::prev()
    {
        if(!isEmpty() and navigationIndex != beginIndex)
        {
            navigationIndex = prevIndex(navigationIndex);
            return data[navigationIndex];
        }
        return {};
    }
}



