# Configuration

Tactos-core-color can be configured by to way. A **configuration** file and a **[DBus](https://www.freedesktop.org/wiki/Software/dbus/) interface**.

## Configuration file

The configuration file uses **YAML** format.

### Window

Optional categorie. Size in pixels of the capture window. Default value 32x32 pixels.

#### witdh

Witdh in pixels of the capture window.

#### height

Height in pixels of the capture window.

### Colors 

List of colors or range of colors that will be detected by Tactos-core-color and wath they trigger.

#### color {#color}

In case of single color, the color to detect. Color is set in **RGBA** 8bits format.
Exemple :
```
color: [0,0,0,255] # white color
```
Exclusive witdh [start](#start) and [end](#end).

#### start {#start}

In case of range color, the minimum of each component of the color. Start is set in **RGBA** 8bits format.
```
start: [0,0,0,255] # white color
```
Exclusive witdh [color](#color).

#### end {#end}

In case of range color, the maximum of each component of the color. End is set in **RGBA** 8bits format.
```
end: [255,255,255,255] # black color
```
Exclusive witdh [color](#color)

#### active

Optional. Colors with active `false` won't be detected. Default `true`.
```
active: false
```
#### name

Optional. Name to find easely a color
```
name: backery
```

#### pins

Optional. Specifie if pins of the module must be raised if the color is detected. Default `true`.
```
pins: false
```

#### speech

Optional categorie. Define speech sentence and triggering.

##### sentence
Sentence to say with speech synthesis or name of the [sound_icon](sound_icon) if `sound_icon` is `true`.
```
sentence: "train station"
```

##### sound_icon {#sound_icon}

Optional. Specifie if the sentence is a real sentence or a `sound_icon`

Some speech synthesis engine can play sound from file in addition to text. Tactos-core-color uses [speech-dispatcher](https://github.com/brailcom/speechd) as middleware for TTS. Speech-dispatcher calls this sound`sound_icon`. A default path for sound_icon folder can be `"/usr/share/sounds/sound-icons`
When trying to play a sound_icon, speech-dispatcher ask TTS engine to play sound with `sentence` filename and say `sentence` as fallback.

```
sentence:  pipe.wav
sound_icon: true
#try to play pipe.wav and say "pipe.wav" as fallback
```

##### trigger

Optional. Type of triggering for sentence. Triggering event are defined in the following enun : \ref Tactos::SPEECH_TRIGGER and use index of the enum.
```
trigger: 0 # speech is triggered immediatly without waiting for event
```

### A complete example

```
Window:
    width: 24
    height: 24
    
Colors:
# detect red color and use default values
# raise pins, dont say any sentence, color is activated
    -  
        color: [255,0,0,255]
# detect green range color and use default values
# raise pins, dont say any sentence, color is activated
    -  
        start: [0,200,0,255]
        end: [0,255,0,255]
    - 
        name: "light blue"
        color [0,0,120,255]
        pins: false # dont raise pins
        speech:
            sentence: backery
            trigger: 1 # trigger on mouse clic
            sound_icon: false # default
    - 
        name: "blue"
        color [0,0,255,255]
        pins: true # default
        speech:
            sentence: pipe
            trigger: 0 # trigger immediatly
            sound_icon: true # use sentence field as sound file name
```

## DBus interface

Tactos-core-color expose a DBus interface to manage configuration on runtime. The interface is the following :

### Properties
- **detectionWin (u8,u8)** [RW] Width and hiegh of the capture window on the screen

### Methods 
- **addColor** : add new color to detect
    - **id (u16)** : unique index of the color
    - **color (u32,u32)** : refer to [start](start) and [end](end)
    - **pins (bool)** : refer to [pins](pins)
    - **speech_trigger (u8)** : refer to [trigger](trigger)
    - **name (string)** :refer to [name](name)
    - **speech (string)** : refer to [speech](speech)
    - **sound_icon (sound_icon)** : refer to [sound_icon](sound_icon)
- **updateColor** : look for existing color and change erase color with new one
    - **id (u16)** : unique index of the color
    - **color (u32,u32)** : refer to [start](start) and [end](end)
    - **pins (bool)** : refer to [pins](pins)
    - **speech_trigger (u8)** : refer to [trigger](trigger)
    - **name (string)** :refer to [name](name)
    - **speech (string)** : refer to [speech](speech)
    - **sound_icon (sound_icon)** : refer to [sound_icon](sound_icon)
- **removeColor** : remove existing color.
    - **color (u32,u32)** : refer to [start](start) and [end](end)
- **loadConfigFile** : load new configuration from file.
    - **file (string)** : file to load configuration from
- **loadConfigList** : load new configuration from List
    - **list (array(q(uu)byssb)** : list of colors as defined by addColor parameters
-**getColors** : return list of current loaded colors (same format as loadConfigList)
	 
See [DBUS_interface.xml](DBus_interface.xml) for more details.
