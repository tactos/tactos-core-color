#include<tactosColor_impl.h>
#include<tuple>
#include<iostream>
#include<filesystem>
#include"config.h"

using namespace Tactos;
/**
 * @brief cast scalar value to uint32_t
 * 
 * @details get scalar of type array<double,4> to uint32_t with 8bits for each channel. reorder scalar 
 *  from BGRA to RGBA 
 * 
 * @param val scalar
 * @return uint32_t
 */
uint32_t scalar2int(cv::Scalar val)
{
    return static_cast<uint32_t>(val[3]) +
    (static_cast<uint32_t>(val[0]) << 8) +
    (static_cast<uint32_t>(val[1]) << 16) +
    (static_cast<uint32_t>(val[2]) << 24);
}

/**
 * @brief cast uint32_t RGBA value to BGRA cv::scalar
 * 
 * @param val uint32_t
 * @return cv::Scalar
 */
cv::Scalar int2scalar(uint32_t val)
{
    cv::Scalar result((val & 0xFF00) >> 8,  (val & 0xFF0000) >> 16, (val & 0xFF000000) >> 24, val & 0xFF);
    return result;
}

/**
 * @brief ColorImpl constructor. call superclass constructor and set Screenrec reference.
 * 
 * @param recorder ScreenRec reference to update.
 */
TactosColorImpl::TactosColorImpl(std::shared_ptr<ScreenRec> recorder)
:colorStub(), m_recorder(recorder){}

/**
 * @brief load new config from file
 * 
 * @param file file to load config from
 * @param invocation dbus generated variable
 */
void TactosColorImpl::loadConfigFile( const Glib::ustring& file, 
                            org::tactos::colorStub::MethodInvocation& invocation)
{
    bool result = this->m_recorder->updateConf(std::filesystem::path(file));
    invocation.ret(result);
}

void TactosColorImpl::loadConfigList(const std::vector<colorTuple_t>& list, org::tactos::colorStub::MethodInvocation& invocation)
{
    std::set<Tactos::rangeColor> colorsList;
    for (auto el : list)
    {
        uint16_t id = std::get<0>(el);
        auto range = std::get<1>(el);
        cv::Scalar start = int2scalar(std::get<0>(range));
        cv::Scalar end = int2scalar(std::get<1>(range));
        bool pins = std::get<2>(el);
        SPEECH_TRIGGER speech_trigger = static_cast<SPEECH_TRIGGER>(std::get<3>(el));
        std::string speech = std::get<4>(el);
        std::string name = std::get<5>(el);
        bool sound_icons = std::get<6>(el);
        colorsList.emplace(start, end, speech, pins, speech_trigger, name, id, sound_icons);
    }
    this->m_recorder->setRangeList(colorsList);
    invocation.ret();
}


/**
 * @brief remove color range from detection getRangeList
 * 
 * @details range color comparaison is only based of start and end value that's why we only need this parameters.'
 * 
 * @param color tuple with start and end stored in uint32_t
 * @param invocation dbus generated variable
 */
void TactosColorImpl::removeColor( const std::tuple<guint32,guint32>& color, 
                             org::tactos::colorStub::MethodInvocation& invocation)
{
    rangeColor col(int2scalar(std::get<0>(color)), int2scalar(std::get<1>(color)));
    bool result = this->m_recorder->removeRange(col);
    invocation.ret(result);
}

/**
 * @brief add new color range to detect
 * 
 * @details return True if success, False otherwise.
 * 
 * @param color tuple with start and end stored in uint32_t
 * @param cells true of pins have to be raised
 * @param speech name of the range said by speech synthesis
 * @param invocation dbus generated variable
 */
void TactosColorImpl::addColor(uint16_t id, const std::tuple< guint32, guint32 >& color, bool pins, uint8_t speech_trigger, const Glib::ustring& speech, const Glib::ustring& name, bool sound_icon, org::tactos::colorStub::MethodInvocation& invocation)
{
    rangeColor col(int2scalar(std::get<0>(color)), 
                   int2scalar(std::get<1>(color)),
                   speech, pins, static_cast<SPEECH_TRIGGER>(speech_trigger), name, id, sound_icon);
    bool result = this->m_recorder->addRange(col);;
    invocation.ret(result);
}

/**
 * @brief update existing range.
 * 
 * @details return True if range found and updated. False otherwise.
 * 
 * @param color tuple with start and end stored in uint32_t (define the range)
 * @param cells true of pins have to be raised
 * @param speech name of the range said by speech synthesis
 * @param invocation dbus generated variable
 */
void TactosColorImpl::updateColor(uint16_t id, const std::tuple< guint32, guint32 >& color, bool pins, uint8_t speech_trigger, const Glib::ustring& speech, const Glib::ustring& name, bool sound_icon, org::tactos::colorStub::MethodInvocation& invocation)
{
    rangeColor col(int2scalar(std::get<0>(color)), 
                   int2scalar(std::get<1>(color)),
                   speech, pins, static_cast<SPEECH_TRIGGER>(speech_trigger), name, id, sound_icon);
    bool result = this->m_recorder->updateRange(col);;
    invocation.ret(result);
}


/**
 * @brief get list of detected ranges.
 * 
 * @param invocation dbus generated value
 */
void TactosColorImpl::getColors(org::tactos::colorStub::MethodInvocation & invocation)
{
    std::vector<colorTuple_t> result;
    for (auto range : this->m_recorder->getRangeList())
    {
        auto colors = std::make_pair(scalar2int(range.start), scalar2int(range.end));
        result.push_back(std::make_tuple(range.index, colors, range.pins, range.speech_trigger,  range.speech, range.name, range.sound_icon));
    }
    invocation.ret(result);
}

/**
 * @brief get detectionWin value
 * 
 * @return std::tuple< guint32, guint32 >
 */
std::tuple<guint8, guint8> TactosColorImpl::detectionWin_get()
{
    auto win = this->m_recorder->getScreenshotSize();
    return static_cast<std::tuple<guint8, guint8>>(win);
}

/**
 * @brief update detectionWin value
 * 
 * @param value new value
 * @return bool
 */
bool TactosColorImpl::detectionWin_setHandler(const std::tuple<guint8, guint8>& value )
{
    this->m_recorder->setScreenshotSize(std::get<0>(value), std::get<1>(value));
    return true;
}

