#include <fstream>
#include "config.h"


namespace YAML {

    template<>
    struct convert<cv::Scalar> {
      static Node encode(const cv::Scalar& rhs) {
        Node node;
        node.push_back(rhs[2]);
        node.push_back(rhs[1]);
        node.push_back(rhs[0]);
        node.push_back(rhs[3]);
        return node;
      }
      
      static bool decode(const Node& node, cv::Scalar& rhs) {
        if(!node.IsSequence() || node.size() != 4) {
          return false;
        }
        rhs.val[0] = node[2].as<double>();
        rhs.val[1] = node[1].as<double>();
        rhs.val[2] = node[0].as<double>();
        rhs.val[3] = node[3].as<double>();
        return true;
      }
    };

  /**
  * @brief add operator << overload for cv::scalar
  * 
  * @param out emitter
  * @param v cv::scalar
  * @return YAML::Emitter&
  */
  Emitter& operator << (Emitter& out, const cv::Scalar& v) {
    out << YAML::Flow;
    out << YAML::BeginSeq << v[0] << v[1] << v[2] << v[3] << YAML::EndSeq;
    return out;
  }
}


/**
* @brief configuration constructor
* 
* @param t_fileName name of yaml file. if nothing is set, configuration will initialize with default values
*/
Tactos::Configuration::Configuration(std::string t_fileName):
yamlName(t_fileName), winSize(32, 32)
{
  if(t_fileName != "")
  {
    this->loadConfiguration();
  }
  
}

/**
* @brief save curent configuration in t_name location
* 
* @param t_name name of the location
*/
void Tactos::Configuration::saveConfiguration(std::string t_name)
{
  this->yamlName = t_name;
  this->saveConfiguration();
}


/**
* @brief save configuration in current yaml file
* 
*/
void Tactos::Configuration::saveConfiguration()
{
  
  YAML::Emitter file;
  
  file << YAML::BeginMap;
  file << YAML::Key <<"Window" << YAML::Value << YAML::BeginMap;
  file << YAML::Key << "width" << YAML::Value << this->winSize.first;
  file << YAML::Key << "height" << YAML::Value << this->winSize.second;
  file << YAML::EndMap;
  file << YAML::Key << "Colors" << YAML::Value << YAML::BeginSeq;
  for (const rangeColor &el : detectRanges)
  {
    file << YAML::BeginMap;
    file << YAML::Key << "start" << YAML::Value << el.start;
    file << YAML::Key << "end" << YAML::Value << el.end;
    if (el.pins)
    {
        file << YAML::Key << "pins" << YAML::Value << el.pins;      
    }
    file << YAML::Key << "speech" << YAML::Value <<  YAML::BeginMap ;
    file << YAML::Key << "sentence" << YAML::Value << el.speech;
    file << YAML::Key << "trigger" << YAML::Value << el.speech_trigger;
    file << YAML::Key << "sound_icon" << YAML::Value << el.sound_icon;
    file << YAML::EndMap;
    file << YAML::EndMap;
  }
  file << YAML::EndSeq << YAML::EndMap;
  std::ofstream fout(this->yamlName);
  fout << file.c_str();
}

/**
* @brief load configuration from file
* 
*/
void Tactos::Configuration::loadConfiguration()
{
  int width = 32;
  int height = 32;
  
  YAML::Node config = YAML::LoadFile(this->yamlName);
  
  if(config["Window"])
  {
    
    width = config["Window"]["width"].as<uint>();
    height = config["Window"]["height"].as<uint>();
  }
  this->winSize = std::make_pair(width, height);
  
  if(config["Colors"])
  {
    
    YAML::Node ranges = config["Colors"];
    cv::Scalar start, end;
    uint16_t index = 0;
    try {
        for (auto range : ranges)
        {
            std::string speech = "";
            bool pins = true;
            std::string name = "";
            SPEECH_TRIGGER  speech_trigger = Tactos::IMMEDIATE;
            bool sound_icon = false;
            if(range["start"])
            {
                start = range["start"].as<cv::Scalar>() ;
                end = range["end"].as<cv::Scalar>() ;
            } else {
                start = range["color"].as<cv::Scalar>() ;
                end = range["color"].as<cv::Scalar>() ;
            }
            if(range["speech"])
            {    
                 auto node_speech = range["speech"];
                 if (node_speech.IsMap())
                 {
                     speech = node_speech["sentence"].as<std::string>();
                     if(node_speech["trigger"])
                     {
                         speech_trigger = static_cast<SPEECH_TRIGGER>(node_speech["trigger"].as<int>());
                     }
                     if(node_speech["sound_icon"])
                     {
                         sound_icon = node_speech["sound_icon"].as<bool>();
                     }
                      
                } else {
                    speech = range["speech"].as<std::string>();
                }
            }
            if(range["pins"])
            {
            pins = range["pins"].as<bool>();
            }
            
            if(range["name"])
            {
            name = range["name"].as<std::string>();
            }
        this->detectRanges.emplace(rangeColor(start, end, speech, pins, speech_trigger, name, index++, sound_icon));
        }
    } catch (YAML::InvalidNode &e)
    {
      std::cerr << "ERROR : Invalid range color detection" << std::endl;
      throw;
    }
  }
}


/**
* @brief configuration destructor.
* 
*/
Tactos::Configuration::~Configuration()
{
}

/**
* @brief return pair with capture window size
* 
* @return std::pair< int width, int height>
*/
std::pair<uint, uint> Tactos::Configuration::getWinSize()
{
  return this->winSize;
}

/**
 * @brief update captured window  size
 * 
 * @param t_winSize new win size
 */
void Tactos::Configuration::setWinSize(std::pair<uint, uint> t_winSize)
{
    this->winSize = t_winSize;
}


/**
* @brief return set of rangesColors detected
* 
* @return std::unordered_set< Tactos::rangeScalar >
*/
std::set< Tactos::rangeColor > Tactos::Configuration::getColors()
{
  return detectRanges;
}

/**
 * @brief set new range of colors
 * 
 * @param colors new list of colors
 */
void Tactos::Configuration::setColors(std::set<rangeColor> colors)
{
    this->detectRanges = colors;
}

