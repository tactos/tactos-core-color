#include"screenrecorder.h"

#include<stdlib.h>

#include <thread>
#include <future>
#include <X11/Xutil.h>
#include <functional>
#include <config.h>

using namespace Tactos;

void add_weight(Tactos::rangeColor range,  std::unordered_map<Tactos::rangeColor, uint8_t>& ranges_weight, uint8_t weight);

/**
* @brief Class in charge of capturing screenshot and analysing it to set matrix values for 
* braille cells
 * @param screenshot size of the area to capture
 * @param matrix size of the resulting matrix
 * @param t_defaultValue if part of the capture area is out of the screen, use this default color
 */
ScreenRec::ScreenRec(mat_size screenshot, mat_size matrix, cv::Scalar t_defaultValue):
  scrshot_size(screenshot), matrix_size(matrix), m_defaultValue(t_defaultValue), m_any(true)
  {
  
  this->m_screenshot = cv::Mat(scrshot_size.height, scrshot_size.width, CV_8UC4, cv::Scalar::all(0));
  // initialize shared memory segment
  this->m_disp = XOpenDisplay(0);
  this->m_root = DefaultRootWindow(this->m_disp);
  
  XWindowAttributes w_attr;
  XGetWindowAttributes(this->m_disp, this->m_root, &w_attr);
  this->m_screen = *w_attr.screen;
  this->m_Ximg = XShmCreateImage(this->m_disp, DefaultVisualOfScreen(&this->m_screen), DefaultDepthOfScreen(&this->m_screen), ZPixmap, NULL, &this->m_XshmInfo, this->scrshot_size.width, this->scrshot_size.height );
  
  this->screen_size.height = this->m_screen.height;
  this->screen_size.width = this->m_screen.width;
  
  
  this->m_XshmInfo.shmid = shmget(IPC_PRIVATE, this->m_Ximg->bytes_per_line*this->m_Ximg->height, IPC_CREAT|0777);
  if(this->m_XshmInfo.shmid < 0)
  {
      throw std::runtime_error("unable to get Xshm_info");
  }
  this->m_XshmInfo.shmaddr = this->m_Ximg->data = (char*)shmat(this->m_XshmInfo.shmid, 0, 0);
  this->m_XshmInfo.readOnly = False;
  if (XShmAttach (this->m_disp, &this->m_XshmInfo) == 0)
  {
      throw std::runtime_error("unable to attach shared memory segment");
  };
  
#ifdef SPEECH_LIB // compile with speechd support only if present
  //create speech dispacher connection
  this->speechCon_current =  spd_open("tactos", "immediate", NULL, SPD_MODE_SINGLE);
  if(this->speechCon_current == NULL)
  {
    throw std::runtime_error("unable to connect to speech synthesis with immediate connexion");
  }
  spd_set_language(this->speechCon_current, "fr");

    this->speechCon_delayed =  spd_open("tactos", "delayed", NULL, SPD_MODE_SINGLE);
  if(this->speechCon_delayed == NULL)
  {
    throw std::runtime_error("unable to connect to speech synthesis with delayed connection");
  }
  spd_set_language(this->speechCon_delayed, "fr");

  
  #endif // SPEECH_LIB
    
  }
  
  bool Tactos::ScreenRec::updateConf(std::filesystem::path file)
{
    if (std::filesystem::is_regular_file(file))
    {
        Configuration conf(file);
        std::pair<int,int> winSize = conf.getWinSize();
        this->setScreenshotSize(winSize.first, winSize.second);
        this->setRangeList(conf.getColors());
        return true;
    }
    return false;
}


/**
* @brief Set value for out of the screen part of screenshot
* 
* @param value value to set
*/
void Tactos::ScreenRec::setDefaultValue(cv::Scalar value)
{
  this->m_defaultValue = value;
}



/**
* @brief Capture image around the x,y position. If part of the capture is out of the screen capture 
* only the part inside the screen and fill the rest with this.fillValue.
* 
* 
* @param x X mouse pointer position
* @param y Y mouse pointer position
*/
void ScreenRec::capture(int x, int y)
{
  std::shared_ptr<scrshot_info> capt_info = std::make_shared<scrshot_info>();
  capt_info->orig_x = x-this->scrshot_size.width/2;
  capt_info->orig_y = y-this->scrshot_size.height/2;
  capt_info->width = this->scrshot_size.width;
  capt_info->height = this->scrshot_size.height;
  
  XImage* img = this->m_Ximg;
  XShmSegmentInfo subXshm_info; // info for sub screenshot capture
  bool isOutOfScreen = this->partOutOfScreen(capt_info);
  if (isOutOfScreen)
  { // create new shared memory segment that have to be removed at the end of the function call
    
    XWindowAttributes w_attr;
    XGetWindowAttributes(this->m_disp, this->m_root, &w_attr);
    img = XShmCreateImage(this->m_disp, DefaultVisualOfScreen(&this->m_screen), DefaultDepthOfScreen(&this->m_screen), ZPixmap, NULL, &subXshm_info, capt_info->width, capt_info->height );
    
    subXshm_info.shmid = shmget(IPC_PRIVATE, img->bytes_per_line*img->height, IPC_CREAT|0777);
    if(subXshm_info.shmid < 0)
    {
      throw std::runtime_error("unable to get Xshm_info");
    }
    subXshm_info.shmaddr = img->data = (char*)shmat(subXshm_info.shmid, 0, 0);
    subXshm_info.readOnly = False;
    if (XShmAttach (this->m_disp, &subXshm_info) == 0)
    {
      throw std::runtime_error("unable to attach shared memory segment");
    };
  }

  // capture image and store it into temporary matrix
  XShmGetImage(this->m_disp, this->m_root, img, capt_info->orig_x, capt_info->orig_y, 0x00ffffff);
  cv::Mat capt_Mat(capt_info->height, capt_info->width, CV_8UC4, img->data);
  
  if (isOutOfScreen)
  { //  create border for out of the screen part
    cv::copyMakeBorder(capt_Mat, this->m_screenshot, capt_info->border_t, capt_info->border_b, capt_info->border_l, capt_info->border_r, cv::BORDER_CONSTANT | cv::BORDER_ISOLATED, this->m_defaultValue);
//       clean shared memory segment
    XShmDetach(this->m_disp, &subXshm_info);
    XDestroyImage(img);
    shmdt (subXshm_info.shmaddr);
    shmctl (subXshm_info.shmid, IPC_RMID, 0);
  } else {
      this->m_screenshot = capt_Mat;
  }
}

/**
 * @brief Check if part of the rectangle captured is out of the screen
 * 
 * @param capt_info special structure containing point of origin, width, height and border values returned by the function
 * @return True if part of rectangle is out of the screen. then store border value in capt_info struct.
 */
bool ScreenRec::partOutOfScreen(std::shared_ptr<scrshot_info> captInfo)
{
  bool outOfBound = false;
  if(captInfo->orig_x + captInfo->width >= static_cast<int>(this->screen_size.width))
  {  
        captInfo->width = this->screen_size.width-captInfo->orig_x;
        captInfo->border_r = this->scrshot_size.width - captInfo->width;
    outOfBound = true;
  } else if (captInfo->orig_x <= 0)
  {  
        captInfo->width += captInfo->orig_x;
        captInfo->border_l = this->scrshot_size.width - captInfo->width;
        captInfo->orig_x  = 0;
    outOfBound = true;
  }
  
  if(captInfo->orig_y + captInfo->height >=static_cast<int>(this->screen_size.height))
  { 
        captInfo->height = this->screen_size.height-captInfo->orig_y;
        captInfo->border_b = this->scrshot_size.height - captInfo->height;
    outOfBound = true;
  } else if (captInfo->orig_y <= 0)
  { // 
        captInfo->height += captInfo->orig_y;
        captInfo->border_t = this->scrshot_size.height - captInfo->height;
        captInfo->orig_y = 0;
    outOfBound = true;
  }
  return outOfBound;
}


/**
* @brief Split local screenshot into n² parts
* 
* @details convenient function, call \ref scrSplit(uint lines, uint cols)
* 
* @param n number of part by side
*/
void ScreenRec::scrSplit(uint n)
{
    scrSplit(n,n);
}


/**
 * @brief Split screenshot into lines*cols parts and store it localy 
* 
* @param lines number of lines to split
* @param cols number of columns to split
*/
void ScreenRec::scrSplit(uint lines, uint cols)
{
    this->m_subScreenShot.clear();
    uint HSquare = this->scrshot_size.height/lines;
    uint WSquare = this->scrshot_size.width/cols;
    for (uint i = 0; i < lines; i++)
    {
        cv::Range HRange(i*HSquare, (i+1)*HSquare);
        for (uint j = 0; j < cols; j++)
        {
            cv::Range WRange(j*WSquare, (j+1)*WSquare);
            this->m_subScreenShot.push_back(cv::Mat(this->m_screenshot(HRange, WRange)));
        }
    }
}

/**
* @brief Test a condition to each part of the screenshot use isInRange by default.
* 
* @param func optional function to test specific condition
* @return std::vector<bool> result for each part of the grid
*/
std::vector<bool> ScreenRec::applyToGrid(std::function<rangeResult (cv::Mat, bool)> func)
{
    if (func == nullptr) {
        func = std::bind(&ScreenRec::isInRange, this, std::placeholders::_1, std::placeholders::_2);
    }
    std::vector<std::shared_ptr<std::future<rangeResult>>> futures;    
    std::vector<bool> result;

    for (const cv::Mat& mat : this->m_subScreenShot)
    {
    auto future = std::make_shared<std::future<rangeResult>>(std::async(std::launch::async,func, mat, m_any));
    futures.push_back(future);
    }
    
    speechs_weight_t speechs_weight;

    for (size_t i = 0; i < futures.size(); ++i)
    {
        futures[i]->wait();
        rangeResult res = futures[i].get()->get();
        result.push_back(res.first);
        uint8_t weight = 1;
        if (i == 5 or i == 6 or i == 9 or i == 10)
        { // future linked to a central case of the grid, should have a more important weight for speech synthesis
            weight = 16;
        }
        for (auto range : res.second)
        {
            add_weight(range, speechs_weight[range.speech_trigger], weight);
        }
    }
    updateSentences(speechs_weight);
    sayCurrentSentence();
   
    lastSpeech = speech_sentence[IMMEDIATE].second;
    return result;
}

/**
 * @brief update store sentence that can be triggered to be said by TTS engine.
 * 
 * @param speechs_weight map of range weight ordered by trigger type
 */
void Tactos::ScreenRec::updateSentences(Tactos::speechs_weight_t& speechs_weight)
{
    for (uint i = 0 ; i < TRIGGER_SIZE; i++)
    {
        auto speech = speechs_weight.find(i);
        if( speech == speechs_weight.end())
        {
            speech_sentence[i] = std::make_pair(false, "");
        } else {
            auto sentence = std::max_element(speech->second.begin(), speech->second.end(),
                                             [](std::pair<Tactos::rangeColor, uint8_t> a,  std::pair<Tactos::rangeColor, uint8_t> b ) {return a.second < b.second;});
            speech_sentence[i] = std::make_pair(sentence->first.sound_icon, sentence->first.speech);
        }
    }
}


/**
 * @brief look for a sentence with IMMEDIATE trigger. If found, say it.
 * 
 */
void Tactos::ScreenRec::sayImmediateSentence()
{
    if (speech_sentence[IMMEDIATE].second != "" and speech_sentence[IMMEDIATE].second != lastSpeech)
    {
        saySentence(speech_sentence[IMMEDIATE].second, speech_sentence[IMMEDIATE].first);
    }
}


/**
 * @brief add weight of a specific range stored in range_weight. if the range didn't exist, create it'
 * 
 * @param range range to look for
 * @param ranges_weight map where weight are stored
 * @param weight weight to add
 */
void add_weight(Tactos::rangeColor range,  std::unordered_map<Tactos::rangeColor, uint8_t>& ranges_weight, uint8_t weight)
{
    if (range.hasSpeech())
    {
        auto el = ranges_weight.find(range);
        if (el != ranges_weight.end())
        {
            el->second += weight;
        } else 
        {
            ranges_weight.insert({range, weight});
        }
    }
}


/**
* @brief Delete shared memory segment, Xlib connection and speech connection
* 
*/
ScreenRec::~ScreenRec()
{
#ifdef SPEECH_LIB // compile with speechd support only if present
  spd_close(this->speechCon_current);
  spd_close(this->speechCon_delayed);
  
#endif // SPEECH_LIB
  XShmDetach(this->m_disp, &this->m_XshmInfo);
  XDestroyImage(this->m_Ximg);
  shmdt (this->m_XshmInfo.shmaddr);
  shmctl (this->m_XshmInfo.shmid, IPC_RMID, 0);
}


/**
* @brief Check if pixels are in a list of  rangeColor
* 
* @param mat image to analyse
* @param any if true (default), check for any pixel in range. If false, check for all pixels in range.
* @return std::pair< raisePin: bool, detectedColors : std::unordered_set< Tactos::rangeColor >> 
 */
std::pair<bool, std::unordered_set<Tactos::rangeColor>> Tactos::ScreenRec::isInRange(cv::Mat mat, bool any) const
{
  std::vector<cv::Mat> channels(4);
  cv::split(mat, channels);
  cv::Mat out;
  std::unordered_set<rangeColor> detectedColors;
  bool pinUp = false;
  
  for (const rangeColor& range : m_rangeList)
  {
    
    bool isInRange = true;
    cv::Mat result = cv::Mat::ones(mat.size(), CV_8UC1);
    for (std::size_t i = 0  ; i < channels.size() ; ++i)
    {
      if (range.start[i] != -1) 
      {
        if(any) // at least one pixel must be in range
        {
          // create a matrix with 0 to all out of range pixels.
          cv::inRange(channels[i], range.start[i], range.end[i], out);
          result &= out; // all channels have to be  in range to validate detection
        } else {
          // all pixel must be in range
          if (!cv::checkRange(channels[i], true, nullptr,  range.start[i], range.end[i]))
          {
            isInRange = false;
            break;
          }
        }
      }
    }
    if(any)
    {
        isInRange = (cv::countNonZero(result) != 0);
    }
    if (isInRange)
    {
      detectedColors.insert(range);
      if (range.pins)
      {
        pinUp = true;
      }
    }
  }
  
  return std::make_pair(pinUp, detectedColors) ;
}


/**
* @brief Set the rangeColor list
* 
* @details this opreation remove any previous list
* 
* @param colorList new list to set
*/
void ScreenRec::setRangeList(std::set<rangeColor> colorList)
{
  this->m_rangeList = colorList;
}

std::set< Tactos::rangeColor > Tactos::ScreenRec::getRangeList()
{
    return m_rangeList;
}

/**
 * @brief Update an existing color range
 * 
 * @details It first try to remove the exixting range and if done then add the new range.
 * 
 * @param updateScalar color range to update
 * @return bool True if found and updated. False otherwise.
 */
bool Tactos::ScreenRec::updateRange(const Tactos::rangeColor updateColor)
{
    
    if(this->removeRange(updateColor))
    {
        return this->addRange(updateColor);
    }
    return false;
}


/**
* @brief Add a new rangeColor to the list
* 
* @param newColor new rangeColor to add
* @return bool True if success. False otherwise.
*/
bool ScreenRec::addRange(rangeColor newColor)
{
  return this->m_rangeList.insert(newColor).second;
}

/**
 * @brief Remove existing color
 * 
 * @param oldColor 
 * @return bool True if success. False otherwise.
 */
bool ScreenRec::removeRange(const rangeColor oldColor)
{
  auto it = this->m_rangeList.find(oldColor);
  if(it != this->m_rangeList.end())
  {
    this->m_rangeList.erase(it);
    return true;
  }
  return false;
}

/**
* @brief Set matrix with a size of lines*cols cells
* 
* @details this method set the number of lines (default 4)and columns 
* and reserve the space needed in the vector containing parts
* of the matrix
* 
* @param lines number of lines [default 4]
* @param cols number of columns 
*/
void Tactos::ScreenRec::setMatrixSize(uint cols, uint lines)
{
  this->matrix_size.height = lines;
  this->matrix_size.width = cols;
  if (this->m_subScreenShot.capacity() >= lines*cols)
  {
    this->m_subScreenShot.clear();
    this->m_subScreenShot.shrink_to_fit();
  }
  this->m_subScreenShot.reserve(lines*cols);
}

/**
 * @brief Update size of the screenshot window
 * 
 * @param w new width
 * @param h new height
 */
void Tactos::ScreenRec::setScreenshotSize(uint w, uint h)
{
    this->scrshot_size.width = w;
    this->scrshot_size.height = h;
}

/**
 * @brief Update size of the screenshot window to square area.
 * 
 * @param n new side length
 */
void Tactos::ScreenRec::setScreenshotSize(uint n)
{
    this->setScreenshotSize(n, n);
}

/**
 * @brief Return screenshot width and hight in a std::pair
 * 
 * @return std::pair< uint, uint >
 */
std::pair<uint, uint> Tactos::ScreenRec::getScreenshotSize()
{
    return std::make_pair(this->scrshot_size.width, this->scrshot_size.height);
}



/**
* @brief Convenient function to call all the process from 
* taking the screenshot to send matrix result to the shared 
* memory manager and text to speech server
* 
* @param x x mouse pointer position
* @param y y mouse pointer position
*/
void Tactos::ScreenRec::processScreenshot(int x, int y)
{
  this->capture(x, y); // capture part of the screen
  this->scrSplit(this->matrix_size.height, this->matrix_size.width); // split image in parts
  auto grid = this->applyToGrid(); // compute each part of the grid
  this->ipc_signal.emit(grid);
}

void Tactos::ScreenRec::saySentence(std::string sentence, bool sound)
{
    
#ifdef SPEECH_LIB // compile with speechd support only if present
    spd_cancel_all(this->speechCon_delayed);
    if (sound)
    {
        spd_sound_icon(this->speechCon_current, SPD_IMPORTANT, sentence.c_str());
    } else 
    {
        spd_say(this->speechCon_delayed, SPD_IMPORTANT, sentence.c_str());
    }
#else
          std::cout << "(speech-dispatcher fallback) [" << sound << "] " << sentence << std::endl;
#endif // SPEECH_LIB
 
}


void Tactos::ScreenRec::sayCurrentSentence(SPEECH_TRIGGER type)
{
    auto sentence = this->speech_sentence[type].second;
    #ifdef SPEECH_LIB // compile with speechd support only if present
    if (!sentence.empty())
    {
        spd_cancel_all(this->speechCon_current);
        if (speech_sentence[type].first)
        {
            spd_sound_icon(this->speechCon_current, SPD_IMPORTANT,
                           sentence.c_str());
        } else 
        {
            spd_say(this->speechCon_current, SPD_IMPORTANT, sentence.c_str());
        }
    }
#else
        std::cout << "(speech-dispatcher fallback) [" << this->speech_sentence[type].first << "] " << sentence << std::endl;
#endif // SPEECH_LIB
}


void Tactos::ScreenRec::saySentencesList(std::vector<speech_t> sentences)
{
    #ifdef SPEECH_LIB // compile with speechd support only if present
    spd_cancel_all(this->speechCon_delayed);
    for (auto sentence : sentences)
    {
        if (sentence.first)
        {
            spd_sound_icon(this->speechCon_current, SPD_IMPORTANT, sentence.second.c_str());
        } else 
        {
            spd_say(this->speechCon_delayed, SPD_IMPORTANT, sentence.second.c_str());
        }
    #else
    std::cout << "(speech-dispatcher fallback) [" << sentence.first << "] " << sentence.second << std::endl;
    #endif // SPEECH_LIB
    }
}

std::array<speech_t, TRIGGER_SIZE> Tactos::ScreenRec::getCurrentSentences()
{
    return speech_sentence;
}


void Tactos::ScreenRec::cancel_speech()
{
#ifdef SPEECH_LIB // compile with speechd support only if present
    spd_stop_all(this->speechCon_current);
#endif // SPEECH_LIB
    
}

