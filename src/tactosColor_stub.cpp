static const char interfaceXml0[] = R"XML_DELIMITER(<node name='/org/tactos/color'>
    <interface name='org.tactos.color'>
        <property type='(yy)' name='detectionWin' access='readwrite'>
            <annotation name="org.freedesktop.DBus.Property.EmitsChangedSignal" value="true"/>
        </property>
        <method name='addColor'>
        	<arg name='id' type='q' direction='in'/>
        	<arg name='color' type='(uu)' direction='in'/>
        	<arg name='pins' type='b' direction='in'/>
        	<arg name='speech_trigger' type='y' direction='in'/>
        	<arg name='speech' type='s' direction='in'/>
        	<arg name='name' type='s' direction='in'/>
        	<arg name='sound_icon' type='b' direction='in'/>            
        	<arg name='success' type='b' direction='out'/>
        </method>
        <method name='updateColor'>
        	<arg name='id' type='q' direction='in'/>
        	<arg name='color' type='(uu)' direction='in'/>
        	<arg name='pins' type='b' direction='in'/>
        	<arg name='speech_trigger' type='y' direction='in'/>
        	<arg name='name' type='s' direction='in'/>
        	<arg name='speech' type='s' direction='in'/>
        	<arg name='sound_icon' type='b' direction='in'/>
        	<arg name='success' type='b' direction='out'/>
        </method>
        <method name='removeColor'>
        	<arg name='color' type='(uu)' direction='in'/>
            <arg name='success' type='b' direction='out'/>
        </method>
        <method name='loadConfigFile'>
        	<arg name='file' type='s' direction='in'/>
        	<arg name='success' type='b' direction='out'/>
        </method>
        <method name='loadConfigList'>
        	<arg name='list' type='a(q(uu)byssb)' direction='in'/>
        </method>
        <method name='getColors'>
			<arg name='list' type='a(q(uu)byssb)' direction='out'/>
		</method>
    </interface>  
</node>
)XML_DELIMITER";

#include "tactosColor_stub.h"

template<class T>
inline T specialGetter(Glib::Variant<T> variant)
{
    return variant.get();
}

template<>
inline std::string specialGetter(Glib::Variant<std::string> variant)
{
    // String is not guaranteed to be null-terminated, so don't use ::get()
    gsize n_elem;
    gsize elem_size = sizeof(char);
    char* data = (char*)g_variant_get_fixed_array(variant.gobj(), &n_elem, elem_size);

    return std::string(data, n_elem);
}

org::tactos::colorStub::colorStub():
    m_interfaceName("org.tactos.color")
{
}

org::tactos::colorStub::~colorStub()
{
    unregister_object();
}

guint org::tactos::colorStub::register_object(
    const Glib::RefPtr<Gio::DBus::Connection> &connection,
    const Glib::ustring &object_path)
{
    if (!introspection_data) {
        try {
            introspection_data = Gio::DBus::NodeInfo::create_for_xml(interfaceXml0);
        } catch(const Glib::Error& ex) {
            g_warning("Unable to create introspection data for %s: %s", object_path.c_str(), ex.what().c_str());
            return 0;
        }
    }

    Gio::DBus::InterfaceVTable *interface_vtable =
        new Gio::DBus::InterfaceVTable(
            sigc::mem_fun(this, &colorStub::on_method_call),
            sigc::mem_fun(this, &colorStub::on_interface_get_property),
            sigc::mem_fun(this, &colorStub::on_interface_set_property));

    guint registration_id;
    try {
        registration_id = connection->register_object(object_path,
            introspection_data->lookup_interface("org.tactos.color"),
            *interface_vtable);
    } catch(const Glib::Error &ex) {
        g_warning("Registration of object %s failed: %s", object_path.c_str(), ex.what().c_str());
        return 0;
    }

    m_registered_objects.emplace_back(RegisteredObject {
        registration_id,
        connection,
        object_path
    });

    return registration_id;
}

void org::tactos::colorStub::unregister_object()
{
    for (const RegisteredObject &obj: m_registered_objects) {
        obj.connection->unregister_object(obj.id);
    }
    m_registered_objects.clear();
}

void org::tactos::colorStub::on_method_call(
    const Glib::RefPtr<Gio::DBus::Connection> &/* connection */,
    const Glib::ustring &/* sender */,
    const Glib::ustring &/* object_path */,
    const Glib::ustring &/* interface_name */,
    const Glib::ustring &method_name,
    const Glib::VariantContainerBase &parameters,
    const Glib::RefPtr<Gio::DBus::MethodInvocation> &invocation)
{
    static_cast<void>(method_name); // maybe unused
    static_cast<void>(parameters); // maybe unused
    static_cast<void>(invocation); // maybe unused

    if (method_name.compare("addColor") == 0) {
        Glib::Variant<guint16> base_id;
        parameters.get_child(base_id, 0);
        guint16 p_id = specialGetter(base_id);

        Glib::Variant<std::tuple<guint32,guint32>> base_color;
        parameters.get_child(base_color, 1);
        std::tuple<guint32,guint32> p_color = specialGetter(base_color);

        Glib::Variant<bool> base_pins;
        parameters.get_child(base_pins, 2);
        bool p_pins = specialGetter(base_pins);

        Glib::Variant<guchar> base_speech_trigger;
        parameters.get_child(base_speech_trigger, 3);
        guchar p_speech_trigger = specialGetter(base_speech_trigger);

        Glib::Variant<Glib::ustring> base_speech;
        parameters.get_child(base_speech, 4);
        Glib::ustring p_speech = specialGetter(base_speech);

        Glib::Variant<Glib::ustring> base_name;
        parameters.get_child(base_name, 5);
        Glib::ustring p_name = specialGetter(base_name);

        Glib::Variant<bool> base_sound_icon;
        parameters.get_child(base_sound_icon, 6);
        bool p_sound_icon = specialGetter(base_sound_icon);

        MethodInvocation methodInvocation(invocation);
        addColor(
            (p_id),
            (p_color),
            (p_pins),
            (p_speech_trigger),
            (p_speech),
            (p_name),
            (p_sound_icon),
            methodInvocation);
    }

    if (method_name.compare("updateColor") == 0) {
        Glib::Variant<guint16> base_id;
        parameters.get_child(base_id, 0);
        guint16 p_id = specialGetter(base_id);

        Glib::Variant<std::tuple<guint32,guint32>> base_color;
        parameters.get_child(base_color, 1);
        std::tuple<guint32,guint32> p_color = specialGetter(base_color);

        Glib::Variant<bool> base_pins;
        parameters.get_child(base_pins, 2);
        bool p_pins = specialGetter(base_pins);

        Glib::Variant<guchar> base_speech_trigger;
        parameters.get_child(base_speech_trigger, 3);
        guchar p_speech_trigger = specialGetter(base_speech_trigger);

        Glib::Variant<Glib::ustring> base_name;
        parameters.get_child(base_name, 4);
        Glib::ustring p_name = specialGetter(base_name);

        Glib::Variant<Glib::ustring> base_speech;
        parameters.get_child(base_speech, 5);
        Glib::ustring p_speech = specialGetter(base_speech);

        Glib::Variant<bool> base_sound_icon;
        parameters.get_child(base_sound_icon, 6);
        bool p_sound_icon = specialGetter(base_sound_icon);

        MethodInvocation methodInvocation(invocation);
        updateColor(
            (p_id),
            (p_color),
            (p_pins),
            (p_speech_trigger),
            (p_name),
            (p_speech),
            (p_sound_icon),
            methodInvocation);
    }

    if (method_name.compare("removeColor") == 0) {
        Glib::Variant<std::tuple<guint32,guint32>> base_color;
        parameters.get_child(base_color, 0);
        std::tuple<guint32,guint32> p_color = specialGetter(base_color);

        MethodInvocation methodInvocation(invocation);
        removeColor(
            (p_color),
            methodInvocation);
    }

    if (method_name.compare("loadConfigFile") == 0) {
        Glib::Variant<Glib::ustring> base_file;
        parameters.get_child(base_file, 0);
        Glib::ustring p_file = specialGetter(base_file);

        MethodInvocation methodInvocation(invocation);
        loadConfigFile(
            (p_file),
            methodInvocation);
    }

    if (method_name.compare("loadConfigList") == 0) {
        Glib::Variant<std::vector<std::tuple<guint16,std::tuple<guint32,guint32>,bool,guchar,Glib::ustring,Glib::ustring,bool>>> base_list;
        parameters.get_child(base_list, 0);
        std::vector<std::tuple<guint16,std::tuple<guint32,guint32>,bool,guchar,Glib::ustring,Glib::ustring,bool>> p_list = specialGetter(base_list);

        MethodInvocation methodInvocation(invocation);
        loadConfigList(
            (p_list),
            methodInvocation);
    }

    if (method_name.compare("getColors") == 0) {
        MethodInvocation methodInvocation(invocation);
        getColors(
            methodInvocation);
    }

}

void org::tactos::colorStub::on_interface_get_property(
    Glib::VariantBase &property,
    const Glib::RefPtr<Gio::DBus::Connection> &/* connection */,
    const Glib::ustring &/* sender */,
    const Glib::ustring &/* object_path */,
    const Glib::ustring &/* interface_name */,
    const Glib::ustring &property_name)
{
    static_cast<void>(property); // maybe unused
    static_cast<void>(property_name); // maybe unused

    if (property_name.compare("detectionWin") == 0) {

        property = Glib::Variant<std::tuple<guchar,guchar>>::create((detectionWin_get()));
    }

}

bool org::tactos::colorStub::on_interface_set_property(
    const Glib::RefPtr<Gio::DBus::Connection> &/* connection */,
    const Glib::ustring &/* sender */,
    const Glib::ustring &/* object_path */,
    const Glib::ustring &/* interface_name */,
    const Glib::ustring &property_name,
    const Glib::VariantBase &value)
{
    static_cast<void>(property_name); // maybe unused
    static_cast<void>(value); // maybe unused

    if (property_name.compare("detectionWin") == 0) {
        try {
            Glib::Variant<std::tuple<guchar,guchar>> castValue =
                Glib::VariantBase::cast_dynamic<Glib::Variant<std::tuple<guchar,guchar>>>(value);
            std::tuple<guchar,guchar> val =
                (specialGetter(castValue));
            detectionWin_set(val);
        } catch (const std::bad_cast &) {
            g_warning ("Bad cast when casting detectionWin");
        }
    }

    return true;
}


bool org::tactos::colorStub::detectionWin_set(const std::tuple<guchar,guchar> & value)
{
    if (detectionWin_setHandler(value)) {
        Glib::Variant<std::tuple<guchar,guchar>> value_get =
            Glib::Variant<std::tuple<guchar,guchar>>::create((detectionWin_get()));

        emitSignal("detectionWin", value_get);
        return true;
    }

    return false;
}

bool org::tactos::colorStub::emitSignal(
    const std::string &propName,
    Glib::VariantBase &value)
{
    std::map<Glib::ustring, Glib::VariantBase> changedProps;
    std::vector<Glib::ustring> changedPropsNoValue;

    changedProps[propName] = value;

    Glib::Variant<std::map<Glib::ustring, Glib::VariantBase>> changedPropsVar =
        Glib::Variant<std::map<Glib::ustring, Glib::VariantBase>>::create(changedProps);
    Glib::Variant<std::vector<Glib::ustring>> changedPropsNoValueVar =
        Glib::Variant<std::vector<Glib::ustring>>::create(changedPropsNoValue);
    std::vector<Glib::VariantBase> ps;
    ps.push_back(Glib::Variant<Glib::ustring>::create(m_interfaceName));
    ps.push_back(changedPropsVar);
    ps.push_back(changedPropsNoValueVar);
    Glib::VariantContainerBase propertiesChangedVariant =
        Glib::Variant<std::vector<Glib::VariantBase>>::create_tuple(ps);

    for (const RegisteredObject &obj: m_registered_objects) {
        obj.connection->emit_signal(
            obj.object_path,
            "org.freedesktop.DBus.Properties",
            "PropertiesChanged",
            Glib::ustring(),
            propertiesChangedVariant);
    }

    return true;
}
