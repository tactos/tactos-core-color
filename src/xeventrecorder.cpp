#include<stdexcept>
#include"xeventrecorder.h"
#include<X11/XKBlib.h>
#include<algorithm>

#include<iostream>

using namespace Tactos;


//detect shift, control, alt, window and alt gr modifiers
uint16_t  MODIFIER_MASK = ShiftMask | ControlMask | Mod1Mask | Mod4Mask | Mod5Mask ;

  /**
  * @brief create new Xevent Recorder
  * 
  * @param t_recEv events to record (default MotionNotify and KeyPress) list available in X.h L180
  * @param t_datumFlags flags to add to recorded events (default XRecordFromClientTime and XRecordFromClientSequence)
  * @param t_clts clients to record, (default XRecordAllClients)
  * @param t_exitKey default key string to quit program with default callback (default 'Escape') (see xstringtokeysym for more details)
  */
  XEventRecorder::XEventRecorder(ScreenRec& t_screenrecorder, std::vector< unsigned int > t_recEv, int t_datumFlags, XRecordClientSpec t_clts, std::string t_exitKey):
  screenrecorder(t_screenrecorder), checkpoints()
{
    
    this->XControlConnexion = XOpenDisplay(0); //open connexion with X server
    if ( this->XControlConnexion == 0 ) {
      throw std::runtime_error ( "unable to open default control connection" );
    }
    
    this->clients = t_clts;
    
    this->XRecordConnection = XOpenDisplay(0); //open connexion with X server
    if ( this->XRecordConnection == 0 ) {
      throw std::runtime_error ( "unable to open default control connection" );
    }
    
    this->controlContext = XRecordCreateContext ( this->XControlConnexion, t_datumFlags, &clients, 1, nullptr, 0);
    XSync ( this->XControlConnexion, true ); // send requests and wait for replies
    
    auto exitSym = XStringToKeysym(t_exitKey.c_str());
    this->addKeyEvent(exitSym, std::bind(&XEventRecorder::stopRecord_clbk, this, std::placeholders::_1, std::placeholders::_2));
    
    
    // selects events to be notified
    for(auto ev_to_rec : t_recEv)
    {
      this->addRecEvent(ev_to_rec);
    }
    XSync(this->XControlConnexion, true);
  }

  /**
  * @brief Add new event to record
  * 
  * @param ev event to record
  */
  void XEventRecorder::addRecEvent(record_event_t ev)
{
      XRecordState *current_context;
      XRecordGetContext ( this->getDisplay(), this->controlContext, &current_context ); //
      bool evAdded = false;
      std::vector<XRecordRange*> ranges;
      if ( current_context->nclients > 0 ) { // we consider that every client has same event to record
          XRecordClientInfo* client = current_context->client_info[0];
          auto ranges_size = client->nranges;
          ranges.reserve(ranges_size);
          for ( long unsigned int i=0; i<ranges_size; i++ ) 
          {
              ranges.push_back(XRecordAllocRange());
            
              const XRecordRange8* current_range;
              XRecordRange8* new_range;
              if ( isDeviceEvent(ev)) { // select event type
                  current_range = &client->ranges[i]->device_events;
                  new_range = &ranges.back()->device_events;
                  ranges.back()->delivered_events = client->ranges[i]->delivered_events;
              } else {
                  current_range = &client->ranges[i]->delivered_events;
                  new_range = &ranges.back()->delivered_events;
                  ranges.back()->device_events = client->ranges[i]->device_events;
              }

              if ( isInRangeOrAdjacent(ev, *current_range) )
              {
                  new_range->first = std::min( ev, static_cast<uint>(current_range->first) );
                  new_range->last = std::max( ev, static_cast<uint>(current_range->last) );
                  evAdded = true;
              } else {
                *new_range = *current_range; // set current range to new one
              }
          }
      }
      
      if (!evAdded) // add new event
      {
        ranges.push_back(XRecordAllocRange());
        if ( isDeviceEvent(ev)) { 
          ranges.back()->device_events.first = ranges.back()->device_events.last = ev; 
        } else {
          ranges.back()->delivered_events.first = ranges.back()->delivered_events.last = ev;         
        }
      }
      XRecordRange** range_array = &ranges[0];
      XRecordRegisterClients ( this->getDisplay(), this->controlContext, current_context->datum_flags, &this->clients, 1, range_array, ranges.size() );
      
      XSync ( this->getDisplay(), true ); // send requests and wait for replies
      
      for (auto range : ranges)
      {
        Xfree(range);
      }
      XRecordFreeState(current_context);
  }

  bool Tactos::XEventRecorder::isDeviceEvent(Tactos::record_event_t ev)
{
    return ev < EnterNotify;
}

bool Tactos::XEventRecorder::isInRangeOrAdjacent(Tactos::record_event_t ev, const XRecordRange8& range)
{
    return ((ev >= static_cast<uint>(range.first-1)) & (ev <= static_cast<uint>(range.last+1)));
}

/**
 * @brief insert callback event triggered by \param key + \param key_mod shortcut
 * 
 * @param key symbol representing key
 * @param clbk callback function with type std::function<void(int,int)>
 * @param key_mod special  key like control or mod1 use by the shortcut
 */
void Tactos::XEventRecorder::addKeyEvent(KeyCode key, Tactos::event_clbk clbk, uint8_t key_mod)
{
    auto shortcut = std::make_pair(key_mod, key);
    this->keyEvents.insert_or_assign(shortcut, clbk);
}

void Tactos::XEventRecorder::addKeyEvent(KeySym key, Tactos::event_clbk clbk, uint8_t key_mod)
{
    KeyCode keycode = XKeysymToKeycode(getDisplay(), key);
    addKeyEvent(keycode, clbk, key_mod);
}


void Tactos::XEventRecorder::stopRecord_clbk(int, int)
{
    this->stopRecord();
}


  
  XRecordContext XEventRecorder::getContext()
{
  return this->controlContext;
}

  
  /**
   * @brief get display associated to Xrecord session.
  * 
  * @return Display*
  */
  Display* XEventRecorder::getDisplay()
  {
    return this->XControlConnexion;
  }
  
  /**
   * @brief add checpoint shortcuts.
   * 
   * @param creation creation shortcut
   * @param next next shortcut
   * @param prev previous shortcut
   */
  void Tactos::XEventRecorder::addCheckpointSupport(shortcut_t creation, shortcut_t next, shortcut_t prev)
{
    this->addKeyEvent(creation.second, std::bind(&XEventRecorder::addCheckpoint, this,  std::placeholders::_1, std::placeholders::_2), creation.first);
    this->addKeyEvent(next.second, std::bind(&XEventRecorder::moveMouseTocheckpoint, this, std::placeholders::_1, std::placeholders::_2, false), next.first);
    this->addKeyEvent(prev.second, std::bind(&XEventRecorder::moveMouseTocheckpoint, this,  std::placeholders::_1, std::placeholders::_2, true), prev.first);
}


  /**
  * @brief get event and call corresponding process.
  * 
  * @details 
  *  - MotionNotify : call ScreenRec::processScreenshot 
  *  - ButtonPress : call ScreenRec::sayCurrentSentence
  *  - KeyPress : call function registered by  addKeyEvent
  * @param dataPtr XEventRecorder object
  * @param ev Event recorded
  */
  void XEventRecorder::eventCallback(XPointer data, XRecordInterceptData* ev)
  {
    int x,y;
    XEventRecorder* xEventRecorder = (XEventRecorder*)(data);
    if (ev->category == XRecordFromServer)
    {
      
      xEvent* event = (xEvent*) ev->data;
      switch(event->u.u.type)
      {
        case MotionNotify:
          x = event->u.keyButtonPointer.rootX;
          y = event->u.keyButtonPointer.rootY;
            xEventRecorder->screenrecorder.processScreenshot(x,y);
          break;
          
        case KeyPress:
          { // create locale variables
            KeyCode key =event->u.u.detail ;
            uint16_t key_mod = (event->u.keyButtonPointer.state &MODIFIER_MASK); 
            x = event->u.keyButtonPointer.rootX;
            y = event->u.keyButtonPointer.rootY;
            XSync(xEventRecorder->getDisplay(), true);
            shortcut_t shortcut = std::make_pair(key_mod, key);
            xEventRecorder->callKeyEvent(x,y, shortcut);
          }
          break;
         
        case ButtonPress:
        {
            auto button = event->u.keyButtonPointer.pad00;
            xEventRecorder->screenrecorder.sayCurrentSentence(Tactos::MOUSE_CLICK); 
            switch(button)
            {
                case BUTTON_CODE::LEFT :
                    xEventRecorder->screenrecorder.sayCurrentSentence(Tactos::LEFT_CLICK);
                    break;
                case BUTTON_CODE::CENTER: 
                    xEventRecorder->screenrecorder.sayCurrentSentence(Tactos::MIDDLE_CLICK);
                    break;
                default: 
                    break;
            };
        }
        break;
          
        case XRecordEndOfData:
          std::cout << "end of data " << std::endl;
          exit(0);
          
        default:
          break;
      }
    }
  }
  
  /**
   * @brief trigger callback linked to shortcut \ref key
   * 
   * @param x mouse x position
   * @param y mouse y position
   * @param key key modifier and key pressed 
   */
  void Tactos::XEventRecorder::callKeyEvent(int x, int y, shortcut_t key)
{
    auto callback = this->keyEvents.find(key);
    if (callback != this->keyEvents.end())
    {
        callback->second(x,y);
    }
}

/**
 * @brief create a new checkpoint at the \ref x, \ref y position. try to find a sentence that can be triggered at this
 * position to name checkpoint
 * 
 * @param x p_x:...
 * @param y p_y:...
 */
void Tactos::XEventRecorder::addCheckpoint(int x, int y)
{
    auto sentences = this->screenrecorder.getCurrentSentences();
    for (auto speech : sentences)
    {
        if (!speech.second.empty())
        {
            checkpoints.add(checkpoint_t(x, y, speech));
            screenrecorder.saySentencesList({{false, "point de sauvegarde"}, speech,  {false, "créé"}});
            return;
        }
    }
    checkpoints.add(checkpoint_t(x,y,{false, ""}));
    screenrecorder.saySentence("point de sauvegarde créé");
}


/**
 * @brief move mouse to previous or next checkpoint created. if the checkpoint doesn't exist, tell it through TTS.'
 * 
 * @param x new x position
 * @param y new y position
 * @param prev get the previous or next point.
 */
void Tactos::XEventRecorder::moveMouseTocheckpoint(int x, int y, bool prev)
{
    
    std::optional checkpoint =  prev ? checkpoints.prev(): checkpoints.next();
    if (checkpoint)
    {
        moveMouse(checkpoint->x, checkpoint->y, x, y);
        screenrecorder.saySentence(checkpoint->speech.second, checkpoint->speech.first);
    } else {
        screenrecorder.saySentence("Le point de sauvegarde n'existe pas.");
    }
}


void Tactos::XEventRecorder::moveMouse(int new_x, int new_y, int x, int y)
{
    int pos_x = new_x - x;
    int pos_y = new_y - y;
    XWarpPointer(this->XControlConnexion, None, None, x, y, 0, 0, pos_x, pos_y);
    XSync(this->XControlConnexion, true);
}




  /**
  * @brief blocking loop for starting recoding events. Set Xpointer callback to 'this'.
  * @throw runtime_error if recording fail
  */
  void XEventRecorder::startRecord()
  {
      
//       std::function<void(char *, XRecordInterceptData*)> callback = std::bind(&XEventRecorder::eventCallback, this, std::placeholders::_1, std::placeholders::_2);
      if ( !XRecordEnableContext ( this->XRecordConnection, this->controlContext,  (XRecordInterceptProc) &XEventRecorder::eventCallback, ( XPointer ) this ) ) {
        throw std::runtime_error ( "unable to enable Xrecord context" );
    }
  }


  /**
  * @brief blocking loop for starting recoding events. 
  * 
  * @param callbackData data to pass to XPointer callback.
  */
  void XEventRecorder::startRecord ( void* callbackData )
  {
    /*  std::function<void(char *, XRecordInterceptData*)> callback = std::bind(&XEventRecorder::eventCallback, this, std::placeholders::_1, std::placeholders::_2);
    */  if (!XRecordEnableContext(this->XRecordConnection, this->controlContext, (XRecordInterceptProc) &XEventRecorder::eventCallback, static_cast<XPointer>(callbackData))) {
          throw std::runtime_error ( "unable to enable Xrecord context" );
      }
  }

  /**
  * @brief close Xrecord and Xlib connexion
  * 
  */
  XEventRecorder::~XEventRecorder()
  {
    std::cout << "disabled context" << std::endl;
    XRecordFreeContext ( this->XRecordConnection, this->controlContext );
    XCloseDisplay ( this->XControlConnexion );
  }
  
  void XEventRecorder::stopRecord()
{
  XRecordDisableContext(this->XControlConnexion, this->controlContext);
  XSync(this->getDisplay(), true);
}
